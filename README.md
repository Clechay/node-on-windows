# Node on Windows
#### Solutions for common problems with using Node.JS on Windows platform 

- [MSBuild connected problems](#msbuild-connected-problems)
  - [TRK0005](#TRK0005)


### MSBuild connected problems
##### TRK0005
error: ```TRACKER : error TRK0005: Failed to locate: "CL.exe". The system cannot find the file specified.```
common log structure:
```
> bufferutil@1.2.1 install <project_dir>\node_modules\bufferutil
> node-gyp rebuild


<project_dir>\node_modules\bufferutil {git}{hg}
{lamb} if not defined npm_config_node_gyp (node "C:\Program Files\nodejs\node_modules\npm\bin\node-gyp-bin\\..\..\node_modules\node-gyp\bin\node-gyp.js" rebuild )  else (node  rebuild )
Building the projects in this solution one at a time. To enable parallel build, please add the "/m" switch.
C:\Program Files (x86)\MSBuild\Microsoft.Cpp\v4.0\V140\Microsoft.CppBuild.targets(366,5): warning MSB8003: Could not find WindowsSDKDir variable from the registry.  TargetF rameworkVersion or PlatformToolset may be set to an invalid version number. [<project_dir>\node_modules\bufferutil\build\bufferutil.vcxproj]
TRACKER : error TRK0005: Failed to locate: "CL.exe". The system cannot find the file specified. [<project_dir>\node_modules\bufferutil\build\buffer util.vcxproj]


gyp ERR! build error
gyp ERR! stack Error: `C:\Program Files (x86)\MSBuild\14.0\bin\msbuild.exe` failed with exit code: 1
gyp ERR! stack     at ChildProcess.onExit (C:\Program Files\nodejs\node_modules\npm\node_modules\node-gyp\lib\build.js:270:23)
gyp ERR! stack     at emitTwo (events.js:87:13)
gyp ERR! stack     at ChildProcess.emit (events.js:172:7)
gyp ERR! stack     at Process.ChildProcess._handle.onexit (internal/child_process.js:200:12)
gyp ERR! System Windows_NT 10.0.10586
gyp ERR! command "C:\\Program Files\\nodejs\\node.exe" "C:\\Program Files\\nodejs\\node_modules\\npm\\node_modules\\node-gyp\\bin\\node-gyp.js" "rebuild"
gyp ERR! cwd <project_dir>\node_modules\bufferutil
gyp ERR! node -v v5.1.0
gyp ERR! node-gyp -v v3.0.3
gyp ERR! not ok
npm WARN install:bufferutil@1.2.1 bufferutil@1.2.1 install: `node-gyp rebuild`
npm WARN install:bufferutil@1.2.1 Exit status 1
```
solution:
* ensure that Microsoft Visual Studio 2015 (Community or not, doesn't matter) is installed (this step may solve the problem)
* execute ```npm config -g set msvs_version 2015``` (this step may solve the problem)
* [ensure that Microsoft Visual Studio 2015 is installed with C++](./visual studio tutorials/cpp installation.md) (this step may solve the problem)
* ensure that Windows SDK is installed (this step may solve the problem)