# C++ installation in MSVS 2015

In order to ensure that Microsoft Visual Studio 2015 is installed with C++:
- download installer for your edition of Microsoft Visual Studio 2015 (eg. community)
- run it
- choose modify
- ensure that c++ is checked (by deafult it is not)
- start installation proces